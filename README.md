# README #

### What is this repository for? ###

* Viola is a 2D roguelike in the style of classic roguelike games such as NetHack. The project is meant to demonstrate procedural content generation and the factory design pattern.
* Version 0.9

### How do I get set up? ###

* Make sure both the Engine and SD4 folders are in the same directory.
* Simply open the Viola .sln file in Visual Studio, and hit compile.

### Who do I talk to? ###

* Repo owner or admin